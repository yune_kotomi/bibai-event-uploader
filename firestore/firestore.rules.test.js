const firebase = require("@firebase/testing");
const fs = require("fs");

const projectId = "bibai-mock";
const rules = fs.readFileSync("../frontend/firestore.rules", "utf8");

describe("firestore-test", () => {
  // はじめに１度ルールを読み込ませる
  beforeAll(
    async () => {
      await firebase.loadFirestoreRules({
        projectId,
        rules
      });
    }
  );

  beforeEach(
    async () => {
      await adminApp.collection('events').doc('event').set({
        title: 'title',
        description: 'description',
        heroImageUrl: 'url',
        hostId: 'host',
        approval: true,
        accept: false,
        publishArtifacts: false,
        publishAuthors: false
      });
    }
  )

  // test毎にデータをクリアする
  afterEach(
    async () => {
      await firebase.clearFirestoreData({ projectId });
    }
  );

  // 全テスト終了後に作成したアプリを全消去
  afterAll(
    async () => {
      await Promise.all(
        firebase.apps().map((app) => app.delete())
      );
    }
  );

  const authedApp = (auth) => {
    return firebase.initializeTestApp({ projectId, auth }).firestore();
  }

  const adminApp = firebase.initializeAdminApp({ projectId }).firestore();
  const guestApp = firebase.initializeTestApp({ projectId }).firestore();
  const hostApp = authedApp({uid: 'host'});
  const userApp1 = authedApp({uid: 'user1'});
  const userApp2 = authedApp({uid: 'user2'});

  describe('events', () => {
    describe('ゲストの読み書き', () => {
      const event = guestApp.collection('events').doc('event');

      test('読み出せる', async () => {
        await firebase.assertSucceeds(event.get());
      });
      test('作成できない', async () => {
        await firebase.assertFails(guestApp.collection('events').add({title: 'title'}));
      });
      test('更新できない', async () => {
        await firebase.assertFails(event.update({title: 'new'}));
      });
    });

    describe('主催者の読み書き', () => {
      const event = hostApp.collection('events').doc('event');
      const params = {
        title: 'title',
        description: 'description',
        heroImageUrl: 'url',
        hostId: 'host',
        approval: true,
        accept: false,
        publishArtifacts: false,
        publishAuthors: false
      }

      test('読み出せる', async () => {
        await firebase.assertSucceeds(event.get());
      });

      test('作成できる', async () => {
        await firebase.assertSucceeds(hostApp.collection('events').add(params));
      });

      test('更新できる', async () => {
        await firebase.assertSucceeds(event.update({title: 'new title'}));
      });

      test('hostIdが自分でないものは作成できない', async () => {
        await firebase.assertFails(hostApp.collection('events').add({...params, hostId: 'wrong'}));
      });

      test('hostIdは変更できない', async () => {
        await firebase.assertFails(event.update({hostId: 'wrong'}));
      })
    });

    describe('ユーザの読み書き', () => {
      const event = userApp1.collection('events').doc('event');

      test('読み出せる', async () => {
        await firebase.assertSucceeds(event.get());
      });

      test('更新できない', async () => {
        await firebase.assertFails(event.update({title: 'new'}));
      });
    });
  });

  describe('users', () => {
    describe('ゲスト', () => {
      const event = guestApp.collection('events').doc('event');
      const users = event.collection('users');

      test('読めない', async () => {
        await firebase.assertFails(users.doc('user1').get())
      });

      test('作成できない', async () => {
        await firebase.assertFails(users.add({name: 'user'}))
      });

      describe('既存ユーザの操作', () => {
        const id = 'existing-user';
        beforeEach(async () => {
          await adminApp.
            collection('events').doc('event').
            collection('users').doc(id).
            set({name: 'user'})
        })

        test('更新できない', async () => {
          await firebase.assertFails(users.doc(id).update({name: 'new'}))
        });
      })
    });

    describe('ユーザ', () => {
      const event = userApp1.collection('events').doc('event');
      const users = event.collection('users');

      describe('未登録の場合', () => {
        test('作成できる', async () => {
          await firebase.assertSucceeds(
            users.doc('user1').set({name: 'user1'})
          );
        });

        test('自分と異なるuidを指定して作成できない', async () => {
          await firebase.assertFails(
            users.doc('user2').set({name: 'user1'})
          );
        });
      });

      describe('参加登録済みの場合', () => {
        const user = users.doc('user1');
        beforeEach(async () => {
          await adminApp.
            collection('events').doc('event').
            collection('users').doc('user1').
            set({name: 'user'})
        });

        test('自分が読める', async () => {
          await firebase.assertSucceeds(user.get())
        });

        test('他のユーザは読めない', async () => {
          await firebase.assertFails(
            userApp2.collection('events').doc('event').
              collection('users').doc('user1').get()
          )
        });

        test('更新できる', async () => {
          await firebase.assertSucceeds(user.update({name: 'new'}))
        });

        test('他のユーザは更新できない', async () => {
          await firebase.assertFails(
            userApp2.collection('events').doc('event').
              collection('users').doc('user1').update({name: 'new'})
          )
        });
      });
    });

    describe('主催者', () => {
      const user = hostApp.
        collection('events').doc('event').
        collection('users').doc('user1');
      beforeEach(async () => {
        await adminApp.
          collection('events').doc('event').
          collection('users').doc('user1').
          set({name: 'user'})
      });

      test('読める', async () => {
        await firebase.assertSucceeds(user.get())
      });

      test('更新できない', async () => {
        await firebase.assertFails(user.update({name: 'new'}))
      });
    });

    describe('messages', () => {
      beforeEach(async () => {
        await adminApp.collection('/events/event/users/user1/messages').doc('usrmsg').set({
          uid: 'user1',
          body: 'message'
        });

        await adminApp.collection('/events/event/users/user1/messages').doc('hostmsg').set({
          uid: 'host',
          body: 'message'
        });
      });

      describe('ユーザ', () => {
        const messages = userApp1.collection('/events/event/users/user1/messages');
        const usrMsg = messages.doc('usrmsg');
        const hostMsg = messages.doc('hostmsg');

        test('読める', async () => {
          await firebase.assertSucceeds(usrMsg.get());
          await firebase.assertSucceeds(hostMsg.get());
        });

        test('作成できる', async () => {
          await firebase.assertSucceeds(messages.add({
            uid: 'user1',
            body: 'message'
          }));
        });

        test('他人のuidは指定できない', async () => {
          await firebase.assertFails(messages.add({
            uid: 'user2',
            body: 'message'
          }));
        });

        test('更新できない', async () => {
          await firebase.assertFails(usrMsg.update({body: 'new'}));
          await firebase.assertFails(hostMsg.update({body: 'new'}));
        });

        test('削除できない', async () => {
          await firebase.assertFails(usrMsg.delete());
          await firebase.assertFails(hostMsg.delete());
        });
      });

      describe('他のユーザ', () => {
        const messages = userApp2.collection('/events/event/users/user1/messages');
        const usrMsg = messages.doc('usrmsg');
        const hostMsg = messages.doc('hostmsg');

        test('読めない', async () => {
          await firebase.assertFails(usrMsg.get());
          await firebase.assertFails(hostMsg.get());
        });

        test('作成できない', async () => {
          await firebase.assertFails(messages.add({
            uid: 'user2',
            body: 'message'
          }));
        });

        test('更新できない', async () => {
          await firebase.assertFails(usrMsg.update({body: 'new'}));
          await firebase.assertFails(hostMsg.update({body: 'new'}));
        });

        test('削除できない', async () => {
          await firebase.assertFails(usrMsg.delete());
          await firebase.assertFails(hostMsg.delete());
        });
      });

      describe('主催者', () => {
        const messages = hostApp.collection('/events/event/users/user1/messages');
        const usrMsg = messages.doc('usrmsg');
        const hostMsg = messages.doc('hostmsg');

        test('読める', async () => {
          await firebase.assertSucceeds(usrMsg.get());
          await firebase.assertSucceeds(hostMsg.get());
        });

        test('作成できる', async () => {
          await firebase.assertSucceeds(messages.add({
            uid: 'host',
            body: 'message'
          }));
        });

        test('他人のuidは指定できない', async () => {
          await firebase.assertFails(messages.add({
            uid: 'user1',
            body: 'message'
          }));
        });

        test('更新できない', async () => {
          await firebase.assertFails(usrMsg.update({body: 'new'}));
          await firebase.assertFails(hostMsg.update({body: 'new'}));
        });

        test('削除できない', async () => {
          await firebase.assertFails(usrMsg.delete());
          await firebase.assertFails(hostMsg.delete());
        });
      });
    });
  });

  describe('artifactMasters', () => {
    beforeEach(async () => {
      await adminApp.collection('events/event/artifactMasters/').doc('am1').
        set({
          title: '新しい投稿',
          description: '',
          objectName: '',
          userName: '',
          afterwords: '',
          uid: 'user1'
        });
    })

    describe('ゲスト', () => {
      const event = guestApp.collection('events').doc('event');
      const am = event.collection('artifactMasters').doc('am1');

      test('読めない', async () => {
        await firebase.assertFails(am.get());
      });

      test('更新できない', async () => {
        await firebase.assertFails(am.update({title: 'new'}));
      });
    });

    describe('オーナー', () => {
      const event = userApp1.collection('events').doc('event');
      const am = event.collection('artifactMasters').doc('am1');

      test('読める', async () => {
        await firebase.assertSucceeds(am.get());
      });

      test('更新できる', async () => {
        await firebase.assertSucceeds(am.update({title: 'new'}));
      });

      test('自分以外のuidは設定できない', async () => {
        await firebase.assertFails(am.update({uid: 'user2'}));
      });

      test('削除できる', async () => {
        await firebase.assertSucceeds(am.delete());
      });
    });

    describe('他のユーザ', () => {
      const event = userApp2.collection('events').doc('event');
      const am = event.collection('artifactMasters').doc('am1');

      test('作成できる', async () => {
        await firebase.assertSucceeds(event.collection('artifactMasters').add({
          title: '新しい投稿',
          description: '',
          objectName: '',
          userName: '',
          afterwords: '',
          uid: 'user2'
        }));
      });

      test('自分以外のuidを設定しては作成できない', async () => {
        await firebase.assertFails(event.collection('artifactMasters').add({
          title: '新しい投稿',
          description: '',
          objectName: '',
          userName: '',
          afterwords: '',
          uid: 'user1'
        }));
      });

      test('読めない', async () => {
        await firebase.assertFails(am.get());
      });

      test('更新できない', async () => {
        await firebase.assertFails(am.update({title: 'new'}));
      });

      test('削除できない', async () => {
        await firebase.assertFails(am.delete());
      });
    });

    describe('主催者', () => {
      const event = hostApp.collection('events').doc('event');
      const am = event.collection('artifactMasters').doc('am1');

      test('読める', async () => {
        await firebase.assertSucceeds(am.get());
      });

      test('更新できない', async () => {
        await firebase.assertFails(am.update({title: 'new'}));
      });

      test('削除できない', async () => {
        await firebase.assertFails(am.delete());
      });
    });
  });

  describe('artifacts', () => {
    beforeEach(async () => {
      await adminApp.collection('events/event/artifactMasters/').doc('am1').
        set({
          title: '新しい投稿',
          description: '',
          objectName: '',
          userName: '',
          afterwords: '',
          uid: 'user1'
        });
      await adminApp.collection('events/event/artifactMasters/').doc('am2').
        set({
          title: '新しい投稿',
          description: '',
          objectName: '',
          userName: '',
          afterwords: '',
          uid: 'user1'
        });

      await adminApp.collection('events/event/artifacts').doc('am1').set({
        title: '新しい投稿',
        description: '',
        objectName: '',
        userName: '',
        afterwords: ''
      });
    });

    describe('投稿非公開時', () => {
      describe('ゲスト', () => {
        const artifacts = guestApp.collection('events/event/artifacts');
        const artifact = artifacts.doc('am1');

        test('読めない', async () => {
          await firebase.assertFails(artifact.get());
        });

        test('追加できない', async () => {
          await firebase.assertFails(artifacts.doc('am2').set({title: 'new'}));
        });

        test('更新できない', async () => {
          await firebase.assertFails(artifact.update({title: 'new'}));
        });

        test('削除できない', async () => {
          await firebase.assertFails(artifact.delete());
        });
      });

      describe('オーナー', () => {
        const artifacts = userApp1.collection('events/event/artifacts');
        const artifact = artifacts.doc('am1');

        test('読めない', async () => {
          await firebase.assertFails(artifact.get());
        });

        test('追加できる', async () => {
          await firebase.assertSucceeds(artifacts.doc('am2').set({title: 'new'}));
        });

        test('更新できる', async () => {
          await firebase.assertSucceeds(artifact.update({title: 'new'}));
        });

        test('削除できる', async () => {
          await firebase.assertSucceeds(artifact.delete());
        });
      });

      describe('ユーザ', () => {
        const artifacts = userApp2.collection('events/event/artifacts');
        const artifact = artifacts.doc('am1');

        test('読めない', async () => {
          await firebase.assertFails(artifact.get());
        });

        test('追加できない', async () => {
          await firebase.assertFails(artifacts.doc('am2').set({title: 'new'}));
        });

        test('更新できない', async () => {
          await firebase.assertFails(artifact.update({title: 'new'}));
        });

        test('削除できない', async () => {
          await firebase.assertFails(artifact.delete());
        });
      });

      describe('主催者', () => {
        const artifacts = hostApp.collection('events/event/artifacts');
        const artifact = artifacts.doc('am1');

        test('読めない', async () => {
          await firebase.assertFails(artifact.get());
        });

        test('追加できない', async () => {
          await firebase.assertFails(artifacts.doc('am2').set({title: 'new'}));
        });

        test('更新できる', async () => {
          await firebase.assertSucceeds(artifact.update({title: 'new'}));
        });

        test('削除できない', async () => {
          await firebase.assertFails(artifact.delete());
        });
      });
    });

    describe('投稿公開時', () => {
      beforeEach(async () => {
        await adminApp.collection('events').doc('event').update({publishArtifacts: true});
      });

      describe('ゲスト', () => {
        const artifacts = guestApp.collection('events/event/artifacts');
        const artifact = artifacts.doc('am1');

        test('読める', async () => {
          await firebase.assertSucceeds(artifact.get());
        });

        test('追加できない', async () => {
          await firebase.assertFails(artifacts.doc('am2').set({title: 'new'}));
        });

        test('更新できない', async () => {
          await firebase.assertFails(artifact.update({title: 'new'}));
        });
      });

      describe('オーナー', () => {
        const artifacts = userApp1.collection('events/event/artifacts');
        const artifact = artifacts.doc('am1');

        test('読める', async () => {
          await firebase.assertSucceeds(artifact.get());
        });

        test('追加できる', async () => {
          await firebase.assertSucceeds(artifacts.doc('am2').set({title: 'new'}));
        });

        test('更新できる', async () => {
          await firebase.assertSucceeds(artifact.update({title: 'new'}));
        });
      });

      describe('ユーザ', () => {
        const artifacts = userApp2.collection('events/event/artifacts');
        const artifact = artifacts.doc('am1');

        test('読める', async () => {
          await firebase.assertSucceeds(artifact.get());
        });

        test('追加できない', async () => {
          await firebase.assertFails(artifacts.doc('am2').set({title: 'new'}));
        });

        test('更新できない', async () => {
          await firebase.assertFails(artifact.update({title: 'new'}));
        });
      });

      describe('主催者', () => {
        const artifacts = hostApp.collection('events/event/artifacts');
        const artifact = artifacts.doc('am1');

        test('読める', async () => {
          await firebase.assertSucceeds(artifact.get());
        });

        test('追加できない', async () => {
          await firebase.assertFails(artifacts.doc('am2').set({title: 'new'}));
        });

        test('更新できる', async () => {
          await firebase.assertSucceeds(artifact.update({title: 'new'}));
        });
      });
    });
  });

  describe('tokens', () => {
    beforeEach(async () => {
      await adminApp.collection('events/event/tokens').doc('user1').set({value: 'token'});
    });

    describe('ゲスト', () => {
      const tokens = guestApp.collection('events/event/tokens');
      const token = tokens.doc('user1');

      test('読めない', async () => {
        await firebase.assertFails(token.get());
      });

      test('追加できない', async () => {
        await firebase.assertFails(tokens.add({value: 't'}));
      });

      test('更新できない', async () => {
        await firebase.assertFails(token.update({value: 't'}));
      });
    });

    describe('ユーザ', () => {
      const tokens = userApp1.collection('events/event/tokens');
      const token = tokens.doc('user1');

      test('読める', async () => {
        await firebase.assertSucceeds(token.get());
      });

      test('追加できない', async () => {
        await firebase.assertFails(tokens.add({value: 't'}));
      });

      test('更新できない', async () => {
        await firebase.assertFails(token.update({value: 't'}));
      });
    });

    describe('別のユーザ', () => {
      const tokens = userApp2.collection('events/event/tokens');
      const token = tokens.doc('user1');

      test('読めない', async () => {
        await firebase.assertFails(token.get());
      });

      test('追加できない', async () => {
        await firebase.assertFails(tokens.add({value: 't'}));
      });

      test('更新できない', async () => {
        await firebase.assertFails(token.update({value: 't'}));
      });
    });
  });
})
