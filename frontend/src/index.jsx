import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom';

import CssBaseline from '@material-ui/core/CssBaseline';

import Home from './home.jsx';
import Event from './event/index.jsx';
import User from './user/index.jsx';
import Footer from './footer.jsx';
import Tos from './tos.jsx';

const app =
  <BrowserRouter>
    <div style={ {minHeight: 'calc(100vh - 92px)'} }>
      <CssBaseline />

      <Switch>
        <Route path='/events/:eventId/:userId' component={User} />
        <Route path='/events/:eventId' component={Event} />
        <Route path='/tos' component={Tos} />
        <Route exact path='/' component={Home} />
      </Switch>
    </div>
    <Footer />
  </BrowserRouter>

ReactDOM.render(app, document.querySelector('#app'));
