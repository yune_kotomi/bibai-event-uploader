export const style =
  {
    title: {
      flexGrow: 1
    },
    heading: {
      fontSize: '30px'
    },
    card: {
      margin: '10px'
    },
    media: {
      height: '400px'
    },
    cardList: {
      listStyleType: 'none',
      padding: 0
    },
    cardLink: {
      textDecoration: 'none'
    },
    remarks: {
      textAlign: 'right',
      fontSize: 'small'
    },
    cardActions: {
      justifyContent: 'flex-end'
    },
    spacer: {
      flexGrow: 1
    },
    buttonField: {
      textAlign: 'center',
      margin: '10px'
    },
    userlistTimestamp: {
      textAlign: 'right',
    },
    userlistName: {
      textAlign: 'left',
      paddingLeft: '16px'
    },
    message: {
      display: 'flex'
    },
    msgElem: {
      margin: '0 8px'
    }
  }
