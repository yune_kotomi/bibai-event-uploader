const BackendSettings = {
  endpoint: 'https://bibai-backend-b4tvhoqrya-uw.a.run.app/',
  s3UrlBase: 'https://bibai-2020-07-31-1.s3.us-west-002.backblazeb2.com/'
}
if (location.hostname === "localhost") {
  BackendSettings.endpoint = 'http://localhost:4567/'
}
export default BackendSettings;
