import React, { useState } from 'react';
import Markdown from 'react-markdown';
import { Link } from 'react-router-dom';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import { style } from '../style';
import BackendSettings from '../backend_settings';
import PublicArtifactLink from './public_artifact_link';
import Timestamp from '../timestamp';

const ArtifactMaster = (props) => {
  const { artifactMaster, event, user } = props;
  const [ author, setAuthor ] = useState(null);
  const classes = makeStyles((theme) => style)();

  event.
    ref.
    collection('users').
    doc(artifactMaster.data().uid).
    get().
    then((doc) => { if (!author) { setAuthor(doc) }});

    const download = () => {
      // B2操作用APIを叩くためにトークンを取得
      fetch(BackendSettings.endpoint + 'token/' + event.ref.id + '/' + user.uid).then((r) => {
        if (r.ok) {
          const tRef = event.ref.collection('tokens').doc(user.uid);
          tRef.get().then((doc) => {
            const token = doc.data().value;
            fetch(BackendSettings.endpoint + 'artifact/' + event.ref.id + '/' + artifactMaster.id + '/download?token=' + token).then((r) => {
              if (r.ok) { r.text().then((u) => location.href = u)}
            })
          })
        }
      });
    }

  return (
    <Card className={ classes.card }>
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          { artifactMaster.data().title }
        </Typography>

        {
          author &&
          <Typography>
            <Link to={ '/events/' + event.id + '/' + author.id } style={ {textDecoration: 'none', pointer: 'cursor'} }>
              { author.data().name }
            </Link>
          </Typography>
        }

        <div>
          <Markdown source={ artifactMaster.data().description } />
        </div>

        <div>
          <Markdown source={ artifactMaster.data().afterwords } />
        </div>

        <div className={ classes.remarks }>
          {
            artifactMaster.data().uploadedTime &&
            <div>
              最終アップロード時刻: <Timestamp t={ artifactMaster.data().uploadedTime } />
            </div>
          }
        </div>
      </CardContent>

      <CardActions className={ classes.cardActions }>
        {
          event.data().publishArtifacts &&
          artifactMaster.data().uploadedTime &&
          <PublicArtifactLink event={ event } artifact={ artifactMaster } />
        }
        {
          !event.data().publishArtifacts &&
          artifactMaster.data().uploadedTime &&
          <Button color="primary" onClick={ download }>
            Download
          </Button>
        }
      </CardActions>
    </Card>
  )
}

export default ArtifactMaster;
