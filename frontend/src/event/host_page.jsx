import React, { useEffect, useState } from 'react';
import { Link, withRouter } from 'react-router-dom';
import _ from 'lodash';
import { firebase, firestore } from '../firebase';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import TextField from '@material-ui/core/TextField';

import ArtifactMaster from './artifact_master';
import Timestamp from '../timestamp';
import BackendSettings from '../backend_settings';

const HostPage = (props) => {
  const { event, setEvent, classes, user } = props;
  const [users, setUsers] = useState([]);
  const [artifactMasters, setArtifactMasters] = useState([]);
  const [artifactText, setArtifactText] = useState(null);

  const redirectToUser = (userId) => {
    props.history.push('/events/' + event.id + '/' + userId)
  }

  event.ref.collection('users').get().then((qs) => {
    if (users.length != qs.docs.length) {
      const users = _.sortBy(qs.docs, (e) => {
        if (e.data().lastMessageTimestamp) {
          return e.data().lastMessageTimestamp.seconds;
        } else {
          return 0;
        }
      }).reverse();
      setUsers(users);

      event.ref.collection('artifactMasters').get().then((qs) => {
        if (artifactMasters.length != qs.docs.length) {
          setArtifactMasters(
            _.sortBy(
              qs.docs,
              (e) => {
                if (e.data().uploadedTime) {
                  return e.data().uploadedTime.seconds;
                } else {
                  return 0;
                }
              }
            ).reverse()
          );

          const texts = _.map(qs.docs, (doc) => {
            const data = doc.data();
            const author = _.find(users, (o) => o.id == data.uid);

            return (
              [
                data.title,
                author.data().name, '',
                data.description, '',
                data.afterwords
              ].join("\n")
            )
          });
          setArtifactText(texts.join("\n\n"));
        }
      });
    }
  });

  const update = (fields) => {
    event.ref.update(fields).then(() => {
      event.ref.get().then((doc) => setEvent(doc));
    })
  }

  const setPublishArtifacts = (v) => {
    // バケットの公開状態を変更
    update({
      publishArtifacts: v,
      artifactsPublishedAt: firebase.firestore.FieldValue.serverTimestamp()
    });

    fetch(BackendSettings.endpoint + 'token/' + event.ref.id + '/' + user.uid).then((r) => {
      if (r.ok) {
        const tRef = event.ref.collection('tokens').doc(user.uid);
        tRef.get().then((doc) => {
          const token = doc.data().value;
          let mode = 'close';
          if (v) { mode = 'publish' }
          fetch(
            BackendSettings.endpoint + 'event/' + event.ref.id + '/' + mode + '?token=' + token,
            { method: 'POST' }
          ).then((r) => {
            if (r.ok) {
              // do nothing
            }
          })
        })
      }
    });
  }

  const setPublishAuthors = (v) => {
    const batch = firestore.batch();
    artifactMasters.map((am) => {
      const diff = {}
      if (v) {
        const author = _.find(users, (o) => o.id == am.data().uid);
        diff.userName = author.data().name;
      } else {
        diff.userName = '';
      }

      const ref = event.ref.collection('artifacts').doc(am.id);
      batch.update(ref, diff);
    });
    batch.commit().then(() => { update({publishAuthors: v}) })
  }

  return (
    <div>
      <Card className={ classes.card }>
        <CardContent>
          <div>
            <FormControlLabel label="参加を受け付ける" control={
                <Switch
                  checked={ event.data().accept }
                  onChange={(e) => update({accept: e.target.checked})  } />
              } />
          </div>
          <div>
            <FormControlLabel label="投稿されたファイルを公開する" control={
                <Switch
                  checked={ event.data().publishArtifacts }
                  onChange={(e) => setPublishArtifacts(e.target.checked) } />
              } />
          </div>
          <div>
            <FormControlLabel label="作者名を公開する" control={
              <Switch
                checked={ event.data().publishAuthors }
                onChange={(e) => setPublishAuthors(e.target.checked) } />
            } />
          </div>
        </CardContent>
      </Card>

      <Card className={ classes.card }>
        <CardContent>
          <Typography color="textSecondary" gutterBottom>
            参加者
          </Typography>

          <table>
            <thead>
              <tr>
                <th>最終メッセージ投稿時刻</th>
                <th className={ classes.userlistName }>名前</th>
              </tr>
            </thead>
            <tbody>
              {
                users.map((user) => {
                  return (
                    <tr　key={ user.id }>
                      <td className={ classes.userlistTimestamp }>
                        {
                          user.data().lastMessageTimestamp &&
                          <Timestamp t={ user.data().lastMessageTimestamp } />
                        }
                      </td>
                      <td className={ classes.userlistName }>
                        <Link to={ '/events/' + event.id + '/' + user.id }>
                          { user.data().name }
                        </Link>
                      </td>
                    </tr>
                  )
                })
              }
            </tbody>
          </table>
        </CardContent>
      </Card>

      <ul className={ classes.cardList }>
        {
          artifactMasters.map((am) => (
            <li key={am.id}>
              <ArtifactMaster
                artifactMaster={ am }
                event={ event }
                user={ user } />
            </li>
          ))
        }
      </ul>

      <Card className={ classes.card }>
        <CardContent>
          {
            artifactText != null &&
            <TextField
              label="転載用表示"
              fullWidth
              multiline
              rowsMax='10'
              defaultValue={ artifactText } />
          }
        </CardContent>
      </Card>
    </div>
  )
}

export default withRouter(HostPage);
