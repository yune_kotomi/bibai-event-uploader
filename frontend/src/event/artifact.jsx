import React, { useState } from 'react';
import Markdown from 'react-markdown';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import { style } from '../style';
import PublicArtifactLink from './public_artifact_link';
import Timestamp from '../timestamp';

const Artifact = (props) => {
  const { artifact, publishAuthors, event } = props;
  const classes = makeStyles((theme) => style)();

  return (
    <Card className={ classes.card }>
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          { artifact.data().title }
        </Typography>

        {
          publishAuthors &&
          <Typography>{ artifact.data().userName }</Typography>
        }

        <div>
          <Markdown source={ artifact.data().description } />
        </div>

        <div className={ classes.remarks }>
          {
            artifact.data().uploadedTime &&
            <div>
              最終アップロード時刻: <Timestamp t={ artifact.data().uploadedTime } />
            </div>
          }
        </div>
      </CardContent>

      <CardActions className={ classes.cardActions }>
        <PublicArtifactLink event={ event } artifact={ artifact } />
      </CardActions>
    </Card>
  )
}

export default Artifact;
