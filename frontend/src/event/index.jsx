import React, { useEffect, useState } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { auth, firestore } from '../firebase';
import _ from 'lodash';

import Markdown from 'react-markdown';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import SettingsIcon from '@material-ui/icons/Settings';
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import Snackbar from '@material-ui/core/Snackbar';

import { style } from '../style';
import EventAppBar from '../event_app_bar';
import Artifact from './artifact';
import HostPage from './host_page'
import LoginButton from '../login_button';

const Event = (props) => {
  const { eventId } = props.match.params;
  const [user, setUser] = useState(null);
  const [event, setEvent] = useState(null)
  const [artifacts, setArtifacts] = useState([]);
  const [editEvent, setEditEvent] = useState(false);
  const [notApprovedOpen, setNotApprovedOpen] = useState(true);
  const [participant, setParticipant] = useState(null);

  const eventRef = firestore.collection('events').doc(eventId);
  eventRef.get().then((doc) => {
    if (event == null && doc.exists) {
      setEvent(doc);
      document.title = doc.data().title;

      if (doc.data().publishArtifacts) {
        eventRef.collection('artifacts').get().then((qs) => {
          if (artifacts.length != qs.docs.length) {
            setArtifacts(
              _.sortBy(
                qs.docs,
                (e) => {
                  if (e.data().uploadedTime) {
                    return e.data().uploadedTime.seconds;
                  } else {
                    return 0;
                  }
                }
              ).reverse()
            );
          }
        })
      }
    }
  });

  auth.onAuthStateChanged((u) => { if (user == null) {
    setUser(u);
    if (u != null) {
      const participantRef = eventRef.collection('users').doc(u.uid);
      participantRef.get().then((doc) => {
        if (doc.exists) { setParticipant(doc) }
      }).catch((e) => {
        console.error(e);
      });
    }
  } });

  const inputRef = {}
  const updateContent = () => {
    const payload =
      {
        title: inputRef.title.value,
        description: inputRef.description.value,
        heroImageUrl: inputRef.heroImageUrl.value
      }
    eventRef.update(payload).then(() => {
      eventRef.get().then((doc) => {
        setEvent(doc);
        setEditEvent(false);
      })
    });
  }

  const joinUser = () => {
    eventRef.collection('users').doc(user.uid).set({
      name: '参加者さん'
    }).then((userRef) => {
      redirectToUser(user.uid)
    }).
    catch((e) => { console.error(e) })
  }

  const redirectToUser = (userId) => {
    props.history.push('/events/' + event.id + '/' + userId)
  }

  const classes = makeStyles((theme) => style)();

  return (
    <div>
      {
        event != null &&
        <div>
          <EventAppBar
            user={ user }
            setUser={ setUser }
            classes={ classes }
            title={ event.data().title } />

          <Container maxWidth="sm">
            {
              !editEvent &&
              <Card className={ classes.card }>
                <CardMedia
                  component="img"
                  className={ classes.media }
                  image={ event.data().heroImageUrl }
                  title={ event.data().title } />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    { event.data().title }
                  </Typography>
                  <Typography color="textSecondary" gutterBottom>
                    開催概要
                  </Typography>
                  <div>
                    <Markdown source={ event.data().description } />
                  </div>
                </CardContent>

                <CardActions className={ classes.cardActions }>
                  {
                    user != null && event.data().hostId == user.uid &&
                    <IconButton onClick={() => { setEditEvent(true) } }>
                      <SettingsIcon />
                    </IconButton>
                  }
                </CardActions>
              </Card>
            }
            {
              editEvent && event.data().hostId == user.uid &&
              <Card className={ classes.card }>
                <CardContent>
                  <div>
                    <TextField
                      label="イベント名"
                      inputRef={(r) => { inputRef.title = r }}
                      fullWidth
                      defaultValue={ event.data().title } />
                  </div>
                  <div>
                    <TextField
                      label="開催概要(Markdown利用可)"
                      multiline
                      inputRef={(r) => { inputRef.description = r }}
                      fullWidth
                      defaultValue={ event.data().description } />
                  </div>
                  <div>
                    <TextField
                      label="ヒーローイメージURL"
                      inputRef={(r) => { inputRef.heroImageUrl = r }}
                      fullWidth
                      defaultValue={ event.data().heroImageUrl } />
                  </div>
                </CardContent>
                <CardActions className={ classes.cardActions }>
                  <IconButton onClick={ updateContent }>
                    <DoneIcon />
                  </IconButton>
                </CardActions>
              </Card>
            }

            {
              event && event.data().approval &&
              <div className={ classes.buttonField }>
                {
                  !user && event.data().accept &&
                  <LoginButton
                    redirectTo={ '/events/' + eventId }
                    variant="contained"
                    color="primary"
                    startIcon={ <Icon>login</Icon> }
                    onClick={() => setShowLogin(true) }
                    label="ログインして参加する" />
                }
                {
                  user && !participant && event.data().accept &&
                  <Button
                    variant="contained"
                    color="primary"
                    startIcon={ <PersonAddIcon /> }
                    onClick={ joinUser }>
                    参加する
                  </Button>
                }
                {
                  user && participant &&
                  <Button
                    variant="contained"
                    color="primary"
                    startIcon={ <CloudUploadIcon /> }
                    onClick={() => { redirectToUser(participant.id) }}>
                    投稿・編集する
                  </Button>
                }
              </div>
            }

            {
              (!user || event.data().hostId != user.uid) &&
              event.data().publishArtifacts &&
              <ul className={classes.cardList}>
                {
                  artifacts.map((artifact) => (
                    <li key={artifact.id}>
                      <Artifact
                        event={ event }
                        artifact={ artifact }
                        publishAuthors={ event.data().publishAuthors } />
                    </li>
                  ))
                }
              </ul>
            }

            {
              user && event.data().hostId == user.uid &&
              <HostPage
                event={ event }
                setEvent={ setEvent }
                classes={ classes }
                user={ user } />
            }
          </Container>
        </div>
      }

      <Snackbar
        open={ event && !event.data().approval && notApprovedOpen }
        onClose={() => { setNotApprovedOpen(false) }}
        message="管理者の開催承認が必要です"
        action={
          <div>
            <IconButton size="small" aria-label="close" color="inherit" onClick={() => { setNotApprovedOpen(false) }}>
              <CloseIcon fontSize="small" />
            </IconButton>
          </div>
        }
      />
    </div>
  )
}

export default withRouter(Event);
