import React from 'react';
import BackendSettings from '../backend_settings';

const PublicArtifactLink = (props) => {
  const { event, artifact } = props;

  if (Date.now()/1000 - event.data().artifactsPublishedAt.seconds < 10 * 60) {
    return <span>ダウンロード可能になるまで最大10分程度お待ち下さい</span>
  } else {
    const url =
      [
        BackendSettings.s3UrlBase + artifact.id,
        artifact.data().objectName
      ].join('/')

    return (
      <a
        href={ url }
        className="MuiButton-textPrimary MuiButton-root"
        style={ {textDecoration: 'none'} }>Download</a>
    )
  }
}

export default PublicArtifactLink;
