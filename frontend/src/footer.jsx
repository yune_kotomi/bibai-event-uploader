import React from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';

const Footer = (props) => {
  const classes = makeStyles((theme) => ({
    footer: {
      display: 'flex',
      padding: "16px 32px",
      color: '#9e9e9e',
      backgroundColor: '#424242',
      flexFlow: 'row wrap'
    },
    links: {
      display: 'flex',
      padding: 0
    },
    linkItem: {
      listStyleType: 'none',
      margin: '0 16px 0 0'
    },
    link: {
      color: 'inherit',
      textDecoration: 'none'
    }
  }))();

  return (
    <footer className={ classes.footer }>
      <div style={ {marginRight: '16px'} }>
        <a href="//ame.yumenosora.net/">
          <img alt="Presented by 雨上がりの青空を探して" srcSet="https://kakera.yumenosora.net/banner234x60@2x.1a4d5f3e.png 2x,https://kakera.yumenosora.net/banner234x60@3x.eabb9125.png 3x,https://kakera.yumenosora.net/banner234x60.c97be9a3.png 1x" src="https://kakera.yumenosora.net/banner234x60.c97be9a3.png" />
        </a>
      </div>

      <ul className={ classes.links }>
        <li className={ classes.linkItem }>
          <Link to={ '/tos' } className={ classes.link }>利用規約・プライバシーポリシー</Link>
        </li>
        <li className={ classes.linkItem }>
          <a href="https://twitter.com/yumenosoranet" className={ classes.link }>Twitter</a>
        </li>
        <li className={ classes.linkItem }>
          <a href="https://gitlab.com/yune_kotomi/bibai-event-uploader" className={ classes.link }>ソースコード</a>
        </li>
      </ul>
    </footer>
  )
}

export default Footer;
