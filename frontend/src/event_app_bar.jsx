import React from 'react';
import { logoutHandler } from './firebase';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Icon from '@material-ui/core/Icon';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const EventAppBar = (props) => {
  const { user, setUser, classes, title, closeButton } = props;

  return (
    <AppBar position="static">
      <Toolbar>
        { closeButton }
        <Typography variant="h6" className={ classes.title }>
          { title }
        </Typography>
        {
          user &&
          <Button
            color="inherit"
            onClick={() => { logoutHandler(setUser) }}>
            <Icon>logout</Icon>
          </Button>
        }
      </Toolbar>
    </AppBar>
  )
}

export default EventAppBar;
