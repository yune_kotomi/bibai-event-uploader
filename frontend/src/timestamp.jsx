import React from 'react';

const Timestamp = (props) => {
  const { t } = props;
  const date = new Date(t.seconds * 1000);
  const zero = (i) => ('00' + i).slice(-2)

  const str =
    date.getFullYear() + '/' +
    [
      date.getMonth() + 1,
      date.getDate()
    ].map((i) => zero(i)).join('/') + ' ' +
    [
      date.getHours(),
      date.getMinutes(),
      date.getSeconds()
    ].map((i) => zero(i)).join(':');

  return (
    <span>{ str }</span>
  )
}

export default Timestamp;
