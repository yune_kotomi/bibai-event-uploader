import React, { useEffect, useState } from 'react';

import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import CircularProgress from '@material-ui/core/CircularProgress';

import BackendSettings from '../backend_settings';

const UploadDialog = (props) => {
  const { open, eventRef, setOpen, user, artifact, setArtifact } = props;
  const [uploading, setUploading] = useState(false);

  const classes = makeStyles((theme) => ({
    buttonField: {
      textAlign: 'center',
      margin: '40px 40px 0',
      width: '250px',
      height: '250px'
    },
    button: {
      width: '250px',
      height: '250px'
    },
    fileInput: {
      opacity: 0,
      appearance: 'none',
      position: 'absolute',
      width: '250px',
      height: '250px'
    },
    circular: {
      marginTop: '105px'
    },
    notice: {
      margin: '10px',
    }
  }))();

  const handleClose = () => {
    setOpen(false);
    setUploading(false);
  }

  const upload = (e) => {
    setUploading(true);

    const file = e.target.files.item(0);

    // B2操作用APIを叩くためにトークンを取得
    fetch(BackendSettings.endpoint + 'token/' + eventRef.id + '/' + user.uid).then((r) => {
      if (r.ok) {
        const tRef = eventRef.collection('tokens').doc(user.uid);
        tRef.get().then((doc) => {
          const token = doc.data().value;
          //取得したトークンでファイルアップロード
          const formData = new FormData();
          formData.append('token', token);
          formData.append('file', file);
          fetch(
            BackendSettings.endpoint + 'artifact/' + eventRef.id + '/' + artifact.id + '/upload',
            { method: 'PUT', body: formData }
          ).then((r) => {
            artifact.ref.get().then((doc) => {
              setArtifact(doc);
              handleClose();
            })
          });
        })
      }
    })
  }

  return (
    <Dialog onClose={ handleClose } open={ open }>
      <div className={ classes.buttonField }>
        {
          !uploading &&
          <IconButton className={ classes.button }>
            <InsertDriveFileIcon />
            <input
              type="file"
              accept=".zip,.nar"
              className={ classes.fileInput }
              onChange={ upload } />
          </IconButton>
        }
        {
          uploading &&
          <CircularProgress className={ classes.circular } />
        }
      </div>
      <div className={ classes.notice }>ファイルをドロップするか、クリックして選択して下さい</div>
    </Dialog>
  )
}
export default UploadDialog;
