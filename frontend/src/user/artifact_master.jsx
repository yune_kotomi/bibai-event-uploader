import React, { useEffect, useState } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { auth, firestore } from '../firebase';
import Markdown from 'react-markdown';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import DeleteIcon from '@material-ui/icons/Delete';
import SettingsIcon from '@material-ui/icons/Settings';
import DoneIcon from '@material-ui/icons/Done';
import TextField from '@material-ui/core/TextField';

import { style } from '../style';
import UploadDialog from './upload_dialog';
import BackendSettings from '../backend_settings';
import Timestamp from '../timestamp';

const ArtifactMaster = (props) => {
  const classes = makeStyles((theme) => style)();
  const { eventRef, removeArtifactMaster, user } = props;
  const [artifactMaster, setArtifactMaster] = useState(props.artifactMaster);
  const [setting, setSetting] = useState(false);
  const [uploadDialogOpen, setUploadDialogOpen] = useState(false);
  const inputRef = {}

  const updateArtifactMaster = () => {
    const batch = firestore.batch();
    const artifactRef = eventRef.collection('artifacts').doc(artifactMaster.id);
    const publicFields =
      {
        title: inputRef.title.value,
        description: inputRef.description.value
      }
    if (event.publishAfterwords) {
      publicFields.afterwords = inputRef.afterwords.value
    }
    batch.update(artifactMaster.ref, {...publicFields, afterwords: inputRef.afterwords.value});
    batch.update(artifactRef, publicFields);

    batch.commit().then(() => {
      artifactMaster.ref.get().then((doc) => {
        setArtifactMaster(doc);
      })
      setSetting(false);
    });
  }

  const download = () => {
    // B2操作用APIを叩くためにトークンを取得
    fetch(BackendSettings.endpoint + 'token/' + eventRef.id + '/' + user.uid).then((r) => {
      if (r.ok) {
        const tRef = eventRef.collection('tokens').doc(user.uid);
        tRef.get().then((doc) => {
          const token = doc.data().value;
          fetch(BackendSettings.endpoint + 'artifact/' + eventRef.id + '/' + artifactMaster.id + '/download?token=' + token).then((r) => {
            if (r.ok) { r.text().then((u) => location.href = u)}
          })
        })
      }
    });
  }

  const remove = () => {
    const batch = firestore.batch();
    const artifactRef = eventRef.collection('artifacts').doc(artifactMaster.id);
    batch.delete(artifactMaster.ref);
    batch.delete(artifactRef);
    batch.commit().then(() => {
      removeArtifactMaster(artifactMaster);
    })
  }

  return (
    <li>
      {
        artifactMaster && !setting &&
        <Card className={ classes.card }>
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              { artifactMaster.data().title }
            </Typography>

            <Typography color="textSecondary" gutterBottom>
              説明
            </Typography>
            <div>
              <Markdown source={ artifactMaster.data().description } />
            </div>

            <Typography color="textSecondary" gutterBottom>
              あとがき
            </Typography>
            <div>
              <Markdown source={ artifactMaster.data().afterwords } />
            </div>

            <div className={ classes.remarks }>
              {
                artifactMaster.data().uploadedTime &&
                <div>
                  最終アップロード時刻: <Timestamp t={ artifactMaster.data().uploadedTime } />
                </div>
              }
            </div>
          </CardContent>

          <CardActions className={classes.cardActions}>
            {
              artifactMaster.data().uid == user.uid &&
              <IconButton onClick={() => {
                if (window.confirm('削除してよろしいですか？')) { remove() } }}>
                <DeleteIcon />
              </IconButton>
            }
            <div className={ classes.spacer } />
            {
              artifactMaster.data().objectName &&
              <IconButton onClick={ download }>
                <CloudDownloadIcon />
              </IconButton>
            }
            {
              artifactMaster.data().uid == user.uid &&
              <div>
                <IconButton onClick={() => { setUploadDialogOpen(true) }}>
                  <CloudUploadIcon />
                </IconButton>
                <UploadDialog
                  open={ uploadDialogOpen }
                  setOpen={ setUploadDialogOpen }
                  user={ user }
                  artifact={ artifactMaster }
                  setArtifact={ setArtifactMaster }
                  eventRef={ eventRef } />
                <IconButton color="primary" onClick={() => { setSetting(true) }}>
                  <SettingsIcon />
                </IconButton>
              </div>
            }
          </CardActions>
        </Card>
      }
      {
        artifactMaster && setting &&
        <Card className={ classes.card }>
          <CardContent>
            <div>
              <TextField
                label="名称"
                inputRef={(r) => { inputRef.title = r }}
                fullWidth
                defaultValue={ artifactMaster.data().title } />
            </div>
            <div>
              <TextField
                label="説明文(Markdown利用可)"
                inputRef={(r) => { inputRef.description = r }}
                fullWidth
                multiline
                defaultValue={ artifactMaster.data().description } />
            </div>
            <div>
              <TextField
                label="あとがき(Markdown利用可)"
                inputRef={(r) => { inputRef.afterwords = r }}
                fullWidth
                multiline
                defaultValue={ artifactMaster.data().afterwords } />
            </div>
          </CardContent>
          <CardActions className={ classes.cardActions }>
            <IconButton onClick={ updateArtifactMaster }>
              <DoneIcon />
            </IconButton>
          </CardActions>
        </Card>
      }
    </li>
  )
}

export default ArtifactMaster;
