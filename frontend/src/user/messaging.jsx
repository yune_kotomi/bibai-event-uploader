import React, { useEffect, useState } from 'react';
import { firebase, firestore } from '../firebase';
import { v4 as uuidv4 } from 'uuid';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import SendIcon from '@material-ui/icons/Send';
import TextField from '@material-ui/core/TextField';

import Timestamp from '../timestamp';

const Messaging = (props) => {
  const { loginUser, userRef, classes, event } = props;
  const inputRef = {}
  const [messages, setMessages] = useState([]);
  userRef.collection('messages').orderBy('timestamp').get().then((qs) => {
    if (qs.docs.length != messages.length) { setMessages(qs.docs) }
  });

  const send = () => {
    const batch = firestore.batch();
    const msgRef = userRef.collection('messages').doc(uuidv4());
    const t = firebase.firestore.FieldValue.serverTimestamp();

    batch.set(msgRef, {
      uid: loginUser.uid,
      body: inputRef.message.value,
      timestamp: t
    });
    // 主催者の書き込みでは最終メッセージのタイムスタンプを更新しない
    if (loginUser.uid != event.data().hostId) {
      batch.update(userRef, {lastMessageTimestamp: t});
    }

    batch.commit().then(() => {
      msgRef.get().then((doc) => {
        inputRef.message.value = '';
        setMessages([...messages, doc]);
      })
    })
  }

  return (
    <Card className={ classes.card }>
      <CardContent>
        <Typography color="textSecondary" gutterBottom>
          メッセージ
        </Typography>
        <ul style={ {padding: 0} }>
          {
            messages.map((msg) => {
              const data = msg.data();
              return (
                <li key={ msg.id } className={ classes.message }>
                  <div className={ classes.msgElem }>
                    <Timestamp t={ data.timestamp } />
                  </div>
                  {
                    data.uid == event.data().hostId &&
                    <div className={ classes.msgElem }>(主催者)</div>
                  }
                  <div className={ classes.msgElem }>{ data.body }</div>
                </li>
              )
            })
          }
        </ul>
      </CardContent>

      <CardActions className={ classes.cardActions }>
        <TextField
          label="メッセージ"
          className={ classes.msgInput }
          fullWidth
          inputRef={(r) => inputRef.message = r }
          onKeyPress={(e) => { if (e.key === 'Enter') send() }} />

        <IconButton onClick={ send }>
          <SendIcon />
        </IconButton>
      </CardActions>
    </Card>
  )
}

export default Messaging;
