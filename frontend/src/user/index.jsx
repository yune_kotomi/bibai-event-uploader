import React, { useEffect, useState } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { auth, firestore } from '../firebase';
import _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';

import Container from '@material-ui/core/Container';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import AddIcon from '@material-ui/icons/Add';
import DoneIcon from '@material-ui/icons/Done';
import SettingsIcon from '@material-ui/icons/Settings';
import Icon from '@material-ui/core/Icon';
import TextField from '@material-ui/core/TextField';

import { style } from '../style';
import EventAppBar from '../event_app_bar';
import ArtifactMaster from '../user/artifact_master';
import Messaging from './messaging';

const User = (props) => {
  const { eventId, userId } = props.match.params;

  const [event, setEvent] = useState(null);
  const [title, setTitle] = useState('');
  const [user, setUser] = useState(null);
  const [userName, setUserName] = useState('');
  const [editUser, setEditUser] = useState(false);
  const [artifactMasters, setArtifactMasters] = useState(null);

  const eventRef = firestore.collection('events').doc(eventId);
  eventRef.get().then((doc) => {
    if (event == null) {
      setEvent(doc);
      const t = doc.data().title + 'への投稿・編集';
      document.title = t;
      setTitle(t);
    }
  });
  const userRef = eventRef.collection('users').doc(userId);
  userRef.get().then((doc) => {
    setUserName(doc.data().name);
  });
  eventRef.collection('artifactMasters').where('uid', '==', userId).get().then((qs) => {
    if (artifactMasters == null) {
      setArtifactMasters(qs.docs);
    }
  });

  auth.onAuthStateChanged((u) => { if (user == null) {
    setUser(u);
  } });

  const classes = makeStyles((theme) => style)();

  let userNameRef;
  const updateUser = () => {
    const name = userNameRef.value;

    userRef.update({name: name}).then((doc) => {
      setEditUser(false);
      setUserName(name);
    });
  }

  const addArtifact = () => {
    const batch = firestore.batch();
    // アクセス制御のため投稿を示す文書は二通り必要
    const id = uuidv4();
    const masterRef = eventRef.collection('artifactMasters').doc(id);
    const publicFields =
      {
        title: '新しい投稿',
        description: '',
        objectName: '',
        userName: '',
        afterwords: ''
      }
    batch.set(masterRef, {...publicFields,
      afterwords: '',
      uid: user.uid
    });
    const publicRef = eventRef.collection('artifacts').doc(id);
    batch.set(publicRef, publicFields);

    batch.commit().then(() => {
      masterRef.get().then((doc) => setArtifactMasters([...artifactMasters, doc]));
    });
  }

  const removeArtifactMaster = (artifactMaster) => {
    const newArtifactMasters = _.reject(artifactMasters, (o) => o.id == artifactMaster.id);
    setArtifactMasters(newArtifactMasters);
  }

  return (
    <div>
      <EventAppBar
        user={ user }
        setUser={ setUser }
        classes={ classes }
        title={ title }
        closeButton={
          <IconButton color="inherit" onClick={() => { props.history.push('/events/' + eventId) }}>
            <CloseIcon />
          </IconButton>
        } />

      <Container maxWidth="sm">
        {
          !editUser &&
          <Card className={ classes.card }>
            <CardContent>
              <Typography color="textSecondary" gutterBottom>
                投稿者名
              </Typography>
              <div>
                { userName }
              </div>
            </CardContent>
            {
              user && user.uid == userId &&
              <CardActions className={ classes.cardActions }>
                <IconButton onClick={() => { setEditUser(true) } }>
                  <SettingsIcon />
                </IconButton>
              </CardActions>
            }
          </Card>
        }
        {
          editUser && user.uid == userId &&
          <Card className={ classes.card }>
            <CardContent>
              <div>
                <TextField
                  label="投稿者名"
                  inputRef={(r) => { userNameRef = r }}
                  fullWidth
                  defaultValue={ userName } />
              </div>
            </CardContent>
            <CardActions className={ classes.cardActions }>
              <IconButton onClick={ updateUser }>
                <DoneIcon />
              </IconButton>
            </CardActions>
          </Card>
        }
        <div>
          <ul className={classes.cardList}>
            {
              artifactMasters &&
              artifactMasters.map((am) => (
                <ArtifactMaster
                  key={ am.id }
                  artifactMaster={ am }
                  eventRef={ eventRef }
                  user={ user }
                  removeArtifactMaster= { removeArtifactMaster } />
              ))
            }
          </ul>

          {
            user && userId == user.uid &&
            <div className={ classes.buttonField }>
              <Button
                variant="contained"
                color="primary"
                className={classes.button}
                startIcon={ <AddIcon /> }
                onClick={ addArtifact }>追加</Button>
            </div>
          }

          {
            user &&
            <Messaging
              loginUser={ user }
              userRef={ userRef }
              classes={ classes }
              event={ event }/>
          }
        </div>
      </Container>
    </div>
  )
}

export default withRouter(User);
