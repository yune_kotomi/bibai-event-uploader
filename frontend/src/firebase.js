import * as app from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import firebaseConfig from "./firebase_config";

app.initializeApp(firebaseConfig);

export const firebase = app;
export const auth = app.auth();
export const firestore = app.firestore();

if (location.hostname === "localhost") {
  firestore.settings({
    host: "localhost:8080",
    ssl: false
  });
}

export const logoutHandler = (setUser) => {
  auth.signOut().
    then((r) => { setUser(null) }).
    catch((e) => { console.log(e) });
}
