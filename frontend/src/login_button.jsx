import React, { useEffect, useState } from 'react';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import { firebase } from './firebase';
import _ from 'lodash';

import Button from '@material-ui/core/Button';

const LoginButton = (props) => {
  const { redirectTo, label } = props;
  const [showLoginUi, setShowLoginUi] = useState(false);

  const config =
    {
      // Popup signin flow rather than redirect flow.
      signInFlow: 'popup',
      // Redirect to /signedIn after sign in is successful. Alternatively you can provide a callbacks.signInSuccess function.
      signInSuccessUrl: '/signedIn',
      // We will display Google and Facebook as auth providers.
      signInOptions: [
        firebase.auth.EmailAuthProvider.PROVIDER_ID,
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      ],
      signInSuccessUrl: redirectTo,
      credentialHelper: 'none'
    }

  const buttonProps = {...props, ...{onClick: (() => setShowLoginUi(true))}}

  return (
    <div>
      {
        !showLoginUi &&
        <Button {...buttonProps}>
          { label }
        </Button>
      }
      {
        showLoginUi &&
        <StyledFirebaseAuth
          uiConfig={ config }
          firebaseAuth={ firebase.auth() } />
      }
    </div>
  )
}

export default LoginButton;
