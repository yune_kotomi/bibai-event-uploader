import React, { useEffect, useState } from 'react';
import { Link, withRouter } from 'react-router-dom';

import { auth, firestore } from './firebase';

import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';

import { style } from './style';
import EventAppBar from './event_app_bar';
import LoginButton from './login_button';

const Home = (props) => {
  const [user, setUser] = useState(null);
  const [events, setEvents] = useState([]);
  const title = '創作物匿名公開イベント用アップローダサービス';

  useEffect(() => {
    document.title = title;
  });

  const classes = makeStyles((theme) => style)();

  // 認証状態の取得
  auth.onAuthStateChanged((user) => setUser(user));

  // 存在するイベントを全て取得
  firestore.collection('events').get().then((qs) => {
    if (events.length != qs.docs.length) {
      setEvents(qs.docs);
    }
  });

  const addEvent = (e) => {
    firestore.collection('events').add({
      title: '新しいイベント',
      description: '',
      heroImageUrl: 'https://placehold.jp/24/cccccc/ffffff/250x250.png?text=no+image',
      hostId: user.uid,
      approval: false,
      accept: false,
      publishArtifacts: false,
      publishAuthors: false,
      publishAfterwords: false
    }).
    then((docRef) => {
      props.history.push('/events/' + docRef.id);
    }).
    catch((e) => { console.error(e) });
  }

  return (
    <div>
      <EventAppBar
        user={ user }
        setUser={ setUser }
        classes={ classes }
        title={ title } />

      <Container maxWidth="sm">
        {events.map((event) => (
          <Link to={ '/events/' + event.id } className={ classes.cardLink } key={event.id}>
            <Card className={ classes.card }>
              <CardMedia
                className={ classes.media }
                image={ event.data().heroImageUrl }
                title={ event.data().title }
                />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  { event.data().title }
                </Typography>
                <Typography>
                  { event.data().description }
                </Typography>
              </CardContent>
            </Card>
          </Link>
        ))}

        <div className={ classes.buttonField }>
          {
            user == null &&
            <LoginButton
              redirectTo='/'
              variant="contained"
              color="primary"
              startIcon={ <Icon>login</Icon> }
              onClick={() => setShowLogin(true) }
              label="ログインしてイベントを作成する" />
          }

          {
            user != null &&
            <Button
              variant="contained"
              color="primary"
              startIcon={ <AddIcon /> }
              onClick={ addEvent }
            >イベントを作成する</Button>
          }
        </div>
      </Container>
    </div>
  )
}

export default withRouter(Home);
