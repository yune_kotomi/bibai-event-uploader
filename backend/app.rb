require 'active_support/all'
require 'sinatra'
require 'sinatra/cors'
require 'jwt'
require 'aws-sdk'
require "google/cloud/firestore"
require 'rest-client'
require "google/cloud/debugger"
use Google::Cloud::Debugger::Middleware

require_relative 'config'

TOKEN_TTL = 1.minute

s3 = Aws::S3::Resource.new(
  :access_key_id => settings.aws_access_key_id,
  :secret_access_key => settings.aws_secret_access_key,
  :region => 'us-west-002',
  :endpoint => 'https://s3.us-west-002.backblazeb2.com'
)

firestore = Google::Cloud::Firestore.new({:project_id => settings.project_name})

set :bind, '0.0.0.0'
set :allow_origin, settings.allow_origin
set :allow_methods, "GET,HEAD,POST,PUT"
set :allow_headers, "content-type"

def authorize_b2
  JSON.parse(
    RestClient::Request.new(
      :method => :get,
      :url => "https://api.backblazeb2.com/b2api/v2/b2_authorize_account",
      :user => settings.aws_access_key_id,
      :password => settings.aws_secret_access_key
    ).execute
  )
end

get '/' do
  'bibai-backend'
end

# BibaiバックエンドAPI認証用のトークンを発行する
get '/token/:event_id/:uid' do
  # トークン生成
  payload = {:uid => params[:uid], :exp => Time.now.since(TOKEN_TTL).to_i}
  token = JWT.encode(payload, settings.secret, 'HS256')

  # 指定されたUIDでFirestoreに書き込み
  ref =
    firestore.
      collection('events').doc(params[:event_id]).
      collection('tokens').doc(params[:uid])
  ref.set({:value => token})

  'ok'
end

# ファイルアップロード
put '/artifact/:event_id/:artifact_id/upload' do
  # トークン検証
  payload = JWT.decode(params[:token], settings.secret).first
  uid = payload['uid']

  # イベント・アーティファクトの取得
  event = firestore.collection('events').doc(params[:event_id])
  artifact_master = event.collection('artifactMasters').doc(params[:artifact_id]).get

  if artifact_master.data[:uid] == uid
    file = params[:file][:tempfile]
    filename = File.basename(params[:file][:filename]).force_encoding('utf-8')

    object = s3.bucket(settings.bucket_name).object(File.join(artifact_master.document_id, filename))
    r = RestClient.put(object.presigned_url(:put), file)

    firestore.batch do
      payload =
        {
          'objectName' => filename,
          'uploadedTime' => firestore.field_server_time
        }
      artifact = event.collection('artifacts').doc(artifact_master.document_id)
      artifact_master.ref.update(payload)
      artifact.update(payload)
    end

    'ok'

  else
    'forbidden'
  end
end

# ダウンロード用signed URL発行
get '/artifact/:event_id/:artifact_id/download' do
  # トークン検証
  payload = JWT.decode(params[:token], settings.secret).first
  uid = payload['uid']

  # イベント・アーティファクトの取得
  event = firestore.collection('events').doc(params[:event_id]).get
  artifact_master = event.ref.collection('artifactMasters').doc(params[:artifact_id]).get

  if artifact_master.data[:uid] == uid || event.data[:hostId] == uid
    # signed url発行
    object = s3.bucket(settings.bucket_name).object(File.join(artifact_master.document_id, artifact_master.data[:objectName]))
    object.presigned_url(:get)
  else
    'forbidden'
  end
end

# ファイル格納バケットのパーミション変更
post '/event/:event_id/:mode' do
  # トークン検証
  payload = JWT.decode(params[:token], settings.secret).first
  uid = payload['uid']

  # イベントの取得
  event = firestore.collection('events').doc(params[:event_id]).get

  # バケット設定変更
  if event.data[:hostId] == uid
    # 主催者のみが可能
    b2 = authorize_b2

    bucket_type =
      if params[:mode] == 'publish'
        'allPublic'
      else
        'allPrivate'
      end

    r =
      RestClient.post(
        "#{b2['apiUrl']}/b2api/v2/b2_update_bucket",
        ({
          'accountId' => b2['accountId'],
          'bucketId' => settings.bucket_id,
          'bucketType' => bucket_type
        }).to_json,
        {:Authorization => b2['authorizationToken']}
      )

    'ok'
  else
    'forbidden'
  end
end
