configure do
  set :secret, ENV['SECRET']
  set :aws_access_key_id, ENV['AWS_ACCESS_KEY_ID']
  set :aws_secret_access_key, ENV['AWS_SECRET_ACCESS_KEY']
  set :bucket_name, ENV['B2_BUCKET_NAME']
  set :bucket_id, ENV['B2_BUCKET_ID']
  set :project_name, ENV['GCP_PROJECT_NAME']
  set :allow_origin, ENV['ALLOW_ORIGIN']
end
