「創作物匿名公開イベント用アップローダサービス」サービス基盤 "Bibai"
====

「創作物匿名公開イベント用アップローダサービス」( https://event.yumenosora.net/ )のサービス提供のためのアプリケーションです。

This program is free software. You can use, modify and redistribute it under the conditions of the GNU Affero General Public License. See COPYING for details.
